We paid our dues no lust for fame 
When tides get turned, they know your name 
When all of you were fast asleep 
We made ourselves the ones to beat 

We were down on our knees 
We were so close to defeat 
Heard a chant, grab your hand 
One thing I ll never understand 
It was magic, magic, hoooo 
Skies are blue, fields are green 
And the crowd was deafening 
Out of breath, ordered in? 
But still we found our way to win 
It was magic, magic hooo 

We run for love we run through hurt 
But gods conspire our fall to earth 
But not before we light the flame 
And feed our hunger for the game 

We were down on our knees 
We were so close to defeat 
Heard a chant, grab your hand 
One thing I ll never understand 
It was magic, magic, hoooo 
Skies are blue, fields are green 
And the crowd was deafening 
Out of breath, ordered in? 
But still we found our way to win 
It was magic, magic hooo 

Aaaah, our blood will run together 
And we won t know surrender, ohh 

We were down on our knees 
We were so close to defeat 
Heard a chant, grab your hand 
One thing I ll never understand 
It was magic, magic, hoooo 
Skies are blue, fields are green 
And the crowd was deafening 
Out of breath, ordered in? 
But still we found our way to win 
It was magic, magic hooo